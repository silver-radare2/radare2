FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > radare2.log'

COPY radare2 .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' radare2
RUN bash ./docker.sh

RUN rm --force --recursive radare2
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD radare2
